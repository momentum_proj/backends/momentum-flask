import pprint
import sys

import prettytable
import requests

from backend.config import LAST_FM_KEY

(artist, title) = (sys.argv[1], sys.argv[2])

url = f"http://ws.audioscrobbler.com/2.0/?method=track.getInfo&track={title}&artist={artist}&api_key={LAST_FM_KEY}&format=json&limit=200"

response = requests.get(url).json()
track = response["track"]
print("Artist: " + track["artist"]["name"])
print("Title: " + track["name"])
print("Cover: " + track["album"]["image"][3]["#text"])