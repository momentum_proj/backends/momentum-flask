import datetime

import mysql.connector

from backend import config


class DatabaseClient():
    _connection = None
    _cursor = None

    def __init__(self):
        self._connection = mysql.connector.connect(
            host=config.DB_HOST,
            user=config.DB_USER,
            password=config.DB_PASSWD,
            database=config.DB_NAME
        )
        self._cursor = self._connection.cursor()


    def authenticate(self, api_key):
        """ Checks wheter or not the client is allowed to interact with the 
        restricted parts of the Momentum api.
        :param api_key: An API key.
        :returns: A status code. """
        # TODO: document codes
        if not api_key:
            return 404

        query = (
            "SELECT status "
            "FROM api_keys "
            "WHERE api_key = %s"
        )
        self._cursor.execute(query, (api_key,))
        rows, _ = self._connection.get_rows()
        if not rows:
            return 401 
        if rows[0][0] == 0:
            return 403
        elif rows[0][0] == 1:
            return 0
        else:
            return 500


    def add_user(self, username, first_name, birth_date, mail, title = None, middle_names = None, last_name = None):
        query = (
            "INSERT INTO users (username, firstName, lastName, birthDate, mail) "
            "VALUES (%s, %s, %s, %s, %s)"
        )
        data = (username, first_name, last_name, birth_date, mail)
        self._cursor.execute(query, data)
        self._connection.commit()
        return 0


    def get_user(self, username):
        query = (
            "SELECT username, title, firstName, lastName, birthDate, mail "
            "FROM users "
            "WHERE username = %s "
        )
        self._cursor.execute(query, (username,))
        rows, _ = self._connection.get_rows()
        if not rows:
            return None 
        return rows[0]


    def get_friends(self, username):
        return None


    def request_friendship(self, user_sender, user_receiver):
        query = (
            "INSERT INTO friendships (sender, receiver) "
            "VALUES ((SELECT id FROM users WHERE username = %s), (SELECT id FROM users WHERE username = %s))"
        )
        self._cursor.execute(query, (user_sender, user_receiver))
        self._connection.commit()
        return 0


    def get_friend_requests(self, username):
        query = (
            "SELECT users.username "
            "FROM friendships, users "
            "WHERE friendships.receiver = (SELECT id FROM users WHERE username = %s) "
            "AND friendships.sender = users.id"
        )
        self._cursor.execute(query, (username,))

        user_ids = []
        for row in self._cursor:
            user_ids.append(row[0])
        return user_ids


    def add_moment(self, username, title, date, description = None, song_artist = None, song_title = None):
        query = (
            "INSERT INTO moments (by_user, creationDate, title, description, songArtist, songTitle) "
            "VALUES ((SELECT id FROM users WHERE username = %s), %s, %s, %s, %s, %s) "
        )
        data = (username, date, title, description, song_artist, song_title)
        self._cursor.execute(query, data)
        self._connection.commit()
        return 0


    def get_moments_by_user(self, username):
        query = (
            "SELECT moments.id "
            "FROM moments, users "
            "WHERE username = %s AND users.id = by_user"
        )
        self._cursor.execute(query, (username,))

        moment_ids = []
        for row in self._cursor:
            # (id,)
            moment_ids.append(row[0])
        
        return moment_ids


    def get_moment(self, moment_id):
        query = (
            "SELECT creationDate, title, description, songArtist, songTitle, location "
            "FROM moments "
            "WHERE id = %s"
        )
        self._cursor.execute(query, (moment_id,))
        rows, _ = self._connection.get_rows()
        return rows[0]