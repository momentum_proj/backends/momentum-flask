import datetime
import json

from flask import Flask, render_template, request

from backend.client import DatabaseClient

app = Flask(__name__)
db_client = DatabaseClient()


# Browser request
@app.route("/")
def index():
    return render_template("index.html")


@app.route("/user/add", methods=["POST"])
def add_user():
    form = dict(request.form)

    auth_code = db_client.authenticate(form.get("api_key"))
    if auth_code:
        return json.dumps(
            { "error": "Not authenticated", "code": auth_code },
            indent=4
        )

    if any(param not in form.keys() for param in ["username", "firstName", "birthDate", "mail"]):
        return json.dumps(
            { "error": "Missing attributes. Required: username, firstName, birthDate, mail" },
            indent=4
        )

    username = form.get("username")
    first_name = form.get("firstName")
    birth_date = form.get("birthDate")
    mail = form.get("mail")
    title = form.get("title")
    middle_names = form.get("middleNames")
    last_name = form.get("lastName")

    response = db_client.add_user(username, first_name, birth_date, mail, title, middle_names, last_name)
    return json.dumps(response, indent=4)


@app.route("/user/<user>")
def get_user(user):
    user = db_client.get_user(user)
    if user:
        # username, title, first_name, last_name, birth_date, mail
        response = {
            "code": 0,
            "username": user[0],
            "firstName": user[2],
            "birthDate": user[4].strftime("%Y-%m-%d"),
            "mail": user[5]
        }

        if user[1]:
            response["title"] = user[1]

        if user[3]:
            response["lastName"] = user[3]
    else:
        response = {
            "code": 404
        }

    return json.dumps(response, indent=4)


@app.route("/user/<user>/moments")
def get_moments(user):
    moments = db_client.get_moments_by_user(user)
    response = {
        "code": 0,
        "moments": moments
    }
    return json.dumps(response, indent=4)


@app.route("/user/<user>/moments/add", methods=["POST"])
def add_moment(user):
    form = request.form

    auth_code = db_client.authenticate(form.get("api_key"))
    if auth_code:
        return json.dumps(
            { "error": "Not authenticated", "code": auth_code },
            indent=4
        )

    result = db_client.add_moment(
        user,
        form.get("title"),
        form.get("date"),
        form.get("description"),
        form.get("songArtist"),
        form.get("songTitle")
    )
    return json.dumps({ "code": result }, indent=4)


@app.route("/user/<user>/moments/<moment_id>")
def get_moment(user, moment_id):
    moment = db_client.get_moment(moment_id)
    # date, title, desc, song_artist, song_title, location
    response = {
        "code": 0,
        "date": moment[0].strftime("%Y-%m-%d"),
        "title": moment[1]
    }

    if moment[2]:
        response["description"] = moment[2]

    if moment[3]:
        response["songArtist"] = moment[3]

    if moment[4]:
        response["songTitle"] = moment[4]

    if moment[5]:
        response["location"] = moment[5]

    return json.dumps(response, indent=4)


# @app.route("/user/<user>/friends")
# def get_friends(user):
#     friends = db_client.get_friends(user)
#     return json.dumps(friends, indent=4)


@app.route("/user/<user>/friends/add", methods=["POST"])
def request_friendship(user):

    auth_code = db_client.authenticate(request.form.get("api_key"))
    if auth_code:
        return json.dumps(
            { "error": "Not authenticated", "code": auth_code },
            indent=4
        )

    user_receiver = request.form.get("username")
    result = db_client.request_friendship(user, user_receiver)
    return json.dumps({ "code": result }, indent=4)


@app.route("/user/<user>/friends/requests")
def get_friend_requests(user):
    friend_requests = db_client.get_friend_requests(user)
    response = {
        "code": 0,
        "friend_requests": friend_requests
    }
    return json.dumps(response, indent=4)
