CREATE TABLE IF NOT EXISTS users(
    id int AUTO_INCREMENT PRIMARY KEY,
    username varchar(32) NOT NULL,
    title varchar(8),
    firstName varchar(32) NOT NULL,
    lastName varchar(32),
    birthDate date NOT NULL,
    mail varchar(128) NOT NULL
);


CREATE TABLE IF NOT EXISTS moments(
    id INT AUTO_INCREMENT PRIMARY KEY,
    by_user int NOT NULL,
    creationDate date NOT NULL,
    title varchar(64) NOT NULL,
    description varchar(256),
    songArtist varchar(64),
    songTitle varchar(64),
    location varchar(64),
    FOREIGN KEY (by_user) REFERENCES users(id)
);


CREATE TABLE IF NOT EXISTS friendships(
    id int AUTO_INCREMENT PRIMARY KEY,
    sender int NOT NULL,
    receiver int NOT NULL,
    accepted bit NOT NULL DEFAULT 0,
    FOREIGN KEY (sender) REFERENCES users(id),
    FOREIGN KEY (receiver) REFERENCES users(id)
);


CREATE TABLE IF NOT EXISTS api_keys(
    api_key varchar(36) NOT NULL DEFAULT UUID() PRIMARY KEY,
    app_name varchar(64) NOT NULL,
    app_desc varchar(128) NOT NULL,
    user_id int,
    status bit NOT NULL DEFAULT 1,
    FOREIGN KEY (user_id) REFERENCES users(id)
);