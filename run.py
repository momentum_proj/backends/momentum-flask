import getopt
import sys

from backend import app

if __name__ == "__main__":
    host = "localhost"
    port = 5000

    (opts, args) = getopt.getopt(sys.argv[1:], "h:p:", ["host=", "port="])
    for (opt, arg) in opts:
        if opt in ("-h", "--host"):
            host = arg
        elif opt in ("-p", "--port"):
            port = arg

    app.run(host=host, port=port)
